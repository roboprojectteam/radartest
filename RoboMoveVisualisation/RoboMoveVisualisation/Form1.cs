﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using System.Collections.Generic;

namespace RoboMoveVisualisation
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		int numOfCells = 40;
		int cellSize;

		int mode = 0; // 0 - default, 1 - set collisions mode, 2 - set robo

		int[][] map;

		struct Robo
		{
			public int x;
			public int y;
			public int direction; // 0 - up, 1 - right, 2 - down, 3 - left

			public void turnRight()
			{
				this.direction = (this.direction + 1) % 4;
			}

			public void turnLeft()
			{
				this.direction = (3 - -this.direction) % 4;
			}
		};

		Bitmap bmp, bmp2;
		Graphics g, r;
		Robo robo;

		private void Form1_Load(object sender, EventArgs e)
		{
			clear();
		}

		private void clear()
		{
			robo.x = numOfCells/2;
			robo.y = numOfCells/2;
			robo.direction = 0;

			cellSize = (pictureBox1.Width / numOfCells + pictureBox1.Height /numOfCells)/2;

			map = new int[numOfCells][]; // 0 - unknown area, 1 - scanned area, 2 - area with obstacle
			for (int i = 0; i < numOfCells; i++)
			{
				map[i] = new int[numOfCells];
			}

			for (int i = 0; i < numOfCells; i++)
			{
				for (int j = 0; j < numOfCells; j++)
				{
					map[i][j] = 0;
				}
			}

			bmp = new Bitmap(pictureBox1.Width, pictureBox1.Height);
			bmp2 = new Bitmap(pictureBox1.Width, pictureBox1.Height);
			g = Graphics.FromImage(bmp);
			r = Graphics.FromImage(bmp2);

			if (isR)
			{
				pictureBox1.Image = bmp2;

			}
			else
			{
				pictureBox1.Image = bmp;

			}

			Pen p = new Pen(Color.Black);

			for (int y = 0; y < numOfCells; ++y)
			{
				g.DrawLine(p, 0, y * cellSize, numOfCells * cellSize, y * cellSize);
				r.DrawLine(p, 0, y * cellSize, numOfCells * cellSize, y * cellSize);
			}

			for (int x = 0; x < numOfCells; ++x)
			{
				g.DrawLine(p, x * cellSize, 0, x * cellSize, numOfCells * cellSize);
				r.DrawLine(p, x * cellSize, 0, x * cellSize, numOfCells * cellSize);
			}


			g.DrawLine(p, numOfCells * cellSize, 0, numOfCells * cellSize, numOfCells * cellSize);
			r.DrawLine(p, numOfCells * cellSize, 0, numOfCells * cellSize, numOfCells * cellSize);
			g.DrawLine(p, 0, numOfCells * cellSize, numOfCells * cellSize, numOfCells * cellSize);
			r.DrawLine(p, 0, numOfCells * cellSize, numOfCells * cellSize, numOfCells * cellSize);

			drawRobo();
			pictureBox1.Invalidate();
		}

		private void pictureBox1_Click(object sender, EventArgs e)
		{
			MouseEventArgs me = (MouseEventArgs)e;
			Point coordinates = me.Location;
			int clickedCellX = coordinates.X / cellSize;
			int clickedCellY = coordinates.Y / cellSize;

			if(this.mode == 1)
			{
				this.fillSquare(Color.Red, clickedCellX, clickedCellY);
			}
			else if (this.mode == 2)
			{
				this.fillSquare(Color.White, robo.x, robo.y);
				robo.x = clickedCellX;
				robo.y = clickedCellY;
				this.drawRobo();
				robo.turnLeft();
			}
		}

		private static String HexConverter(Color c)
		{
			return (c.A.ToString("X2") + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2")).ToLower();
		}

		private void pictureBox1_DoubleClick(object sender, EventArgs e)
		{
			MouseEventArgs me = (MouseEventArgs)e;
			Point coordinates = me.Location;
			int clickedCellX = coordinates.X / cellSize;
			int clickedCellY = coordinates.Y / cellSize;

			if (this.mode == 1)
			{
				this.fillSquare(Color.White, clickedCellX, clickedCellY);
			}
		}

		private void drawRobo()
		{

			g = Graphics.FromImage(bmp);
			r = Graphics.FromImage(bmp2);
			if (isR)
			{
				pictureBox1.Image = bmp2;

			}
			else
			{
				pictureBox1.Image = bmp;

			}

			g.FillRectangle(new SolidBrush(Color.DarkBlue), robo.x * cellSize + 1, robo.y * cellSize + 1, cellSize - 1, cellSize - 1);
			r.FillRectangle(new SolidBrush(Color.DarkBlue), robo.x * cellSize + 1, robo.y * cellSize + 1, cellSize - 1, cellSize - 1);

			SolidBrush p = new SolidBrush(Color.YellowGreen);

			float part = cellSize / 10;

			switch (robo.direction)
			{
				case 0:
					g.FillRectangle(p, robo.x * cellSize + part, robo.y * cellSize + part, part, part);
					r.FillRectangle(p, robo.x * cellSize + part, robo.y * cellSize + part, part, part);
					g.FillRectangle(p, robo.x * cellSize + cellSize - 2*part, robo.y * cellSize + part, part, part);
					r.FillRectangle(p, robo.x * cellSize + cellSize - 2*part, robo.y * cellSize + part, part, part);
					break;
				case 1:
					g.FillRectangle(p, robo.x * cellSize + cellSize - 2*part, robo.y * cellSize + part, part, part);
					r.FillRectangle(p, robo.x * cellSize + cellSize - 2*part, robo.y * cellSize + part, part, part);
					g.FillRectangle(p, robo.x * cellSize + cellSize - 2*part, robo.y * cellSize + cellSize - 2*part, part, part);
					r.FillRectangle(p, robo.x * cellSize + cellSize - 2*part, robo.y * cellSize + cellSize - 2*part, part, part);
					break;
				case 2:
					g.FillRectangle(p, robo.x * cellSize + part, robo.y * cellSize + cellSize - 2*part, part, part);
					r.FillRectangle(p, robo.x * cellSize + part, robo.y * cellSize + cellSize - 2*part, part, part);
					g.FillRectangle(p, robo.x * cellSize + cellSize - 2*part, robo.y * cellSize + cellSize - 2*part, part, part);
					r.FillRectangle(p, robo.x * cellSize + cellSize - 2*part, robo.y * cellSize + cellSize - 2*part, part, part);
					break;
				default:
					g.FillRectangle(p, robo.x * cellSize + part, robo.y * cellSize + part, part, part);
					r.FillRectangle(p, robo.x * cellSize + part, robo.y * cellSize + part, part, part);
					g.FillRectangle(p, robo.x * cellSize + part, robo.y * cellSize + cellSize - 2*part, part, part);
					r.FillRectangle(p, robo.x * cellSize + part, robo.y * cellSize + cellSize - 2*part, part, part);
					break;
			}
		}

		private void fillSquare(Color c, int x, int y)
		{
			g = Graphics.FromImage(bmp);
			r = Graphics.FromImage(bmp2);
			if (isR)
			{
				pictureBox1.Image = bmp2;

			}
			else
			{
				pictureBox1.Image = bmp;

			}
			//pictureBox1.Image = bmp;

			SolidBrush p = new SolidBrush(c);

			g.FillRectangle(p, x * cellSize + 1, y * cellSize + 1, cellSize - 1, cellSize - 1);

			if (isR)
			{
				r.FillRectangle(p, x * cellSize + 1, y * cellSize + 1, cellSize - 1, cellSize - 1);
			}
		}

		private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			this.mode = 1;
		}

		private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			this.mode = 2;
		}

		private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			this.mode = 0;
			this.stop = false;
			this.moveRobotAlgorithm();
		}

		int cantMoveCount = 0;
		bool isBasicMove = true;
		bool isCycled = false;
		bool stop = false;

		private void moveRobotAlgorithm()
		{
			map[robo.y][robo.x] = 2;
			while (!scanDone())
			{
				isCycled = false;
				cantMoveCount = 0;
				this.isBasicMove = true;

				if (!baseMove())
				{
					this.isBasicMove = false;
					anotherWay();
				}
			}
			Console.WriteLine("Done!");
		}

		private void anotherWay()
		{
			List<Cell> path = new List<Cell>();
			path = this.FindWave(robo.x, robo.y);
			if (goToPoint(path) == false && !stop)
			{
				anotherWay();
			}
			else
			{
				moveRobotAlgorithm();
			}
		}

		private Boolean goToPoint(List<Cell> path)
		{
			foreach (Cell cell in path)
			{
				scan();
				setRobotDirection(cell);
				if (canMove())
				{
					move();
					scan();
				}
				else
				{
					return false;
				}
			}
			return true;
		}

		private Boolean baseMove()
		{
			while (!scanDone())
			{
				if (++cantMoveCount == 5) { break; }

				scan();
				if (canMove() && !isCycled)
				{
					cantMoveCount = 0;
					move();
					scan();
				}
			}

			if (scanDone())
			{
				return true;
			}

			return false;
		}

		private void setRobotDirection(Cell cell)
		{
			int _relation; // 0 - up from robo pos, 1 - right, 2 -down, 3 - left

			if(robo.x == cell.j)
			{
				if(robo.y > cell.i)
				{
					_relation = 0;
				}
				else
				{
					_relation = 2;
				}
			}
			else
			{
				if(robo.x > cell.j)
				{
					_relation = 3;
				}
				else
				{
					_relation = 1;
				}
			}

			robo.direction = _relation;

			drawRobo();
		}

		private void scan()
		{
			scanNearestPositions();
		}

		private void scanNearestPositions()
		{
			int roboCenterX = robo.x * cellSize + cellSize / 2;
			int roboCenterY = robo.y * cellSize + cellSize / 2;

			bool up;
			bool right;
			bool down;
			bool left;

			if(robo.x != numOfCells -1)
			{
				right = checkIsObstacleSquare(roboCenterX + cellSize, roboCenterY);
				if (right)
				{
					map[robo.y][robo.x + 1] = 3;
					this.fillSquare(Color.Red, robo.x+1, robo.y);
				}
				else if (map[robo.y][robo.x + 1] != 2)
				{
					this.fillSquare(Color.Aqua, robo.x+1, robo.y);
					map[robo.y][robo.x + 1] = 1;
				}

				if (robo.y != numOfCells-1)
				{
					down = checkIsObstacleSquare(roboCenterX, roboCenterY + cellSize);
					if (down)
					{
						map[robo.y+1][robo.x] = 3;
						this.fillSquare(Color.Red, robo.x, robo.y + 1);
					}
					else if (map[robo.y + 1][robo.x] != 2)
					{
						this.fillSquare(Color.Aqua, robo.x, robo.y+1);
						map[robo.y+1][robo.x] = 1;
					}
				}

				if (robo.y != 0)
				{
					up = checkIsObstacleSquare(roboCenterX, roboCenterY - cellSize);
					if (up)
					{
						map[robo.y-1][robo.x] = 3;
						this.fillSquare(Color.Red, robo.x, robo.y - 1);
					}
					else if (map[robo.y -1][robo.x] != 2)
					{
						this.fillSquare(Color.Aqua, robo.x, robo.y-1);
						map[robo.y-1][robo.x] = 1;
					}
				}

				if (robo.x != 0)
				{
					left = checkIsObstacleSquare(roboCenterX - cellSize, roboCenterY);
					if (left)
					{
						map[robo.y][robo.x-1] = 3;
						this.fillSquare(Color.Red, robo.x-1, robo.y);
					}
					else if (map[robo.y][robo.x - 1] != 2)
					{
						this.fillSquare(Color.Aqua, robo.x-1, robo.y);
						map[robo.y][robo.x-1] = 1;
					}
				}
			}
			else if (robo.x != 0)
			{
				left = checkIsObstacleSquare(roboCenterX - cellSize, roboCenterY);
				if (left)
				{
					map[robo.y][robo.x - 1] = 3;
					this.fillSquare(Color.Red, robo.x-1, robo.y);
				}
				else if (map[robo.y][robo.x - 1] != 2)
				{
					this.fillSquare(Color.Aqua, robo.x-1, robo.y);
					map[robo.y][robo.x - 1] = 1;
				}
				

				if (robo.y != numOfCells - 1)
				{
					down = checkIsObstacleSquare(roboCenterX, roboCenterY + cellSize);
					if (down)
					{
						map[robo.y + 1][robo.x] = 3;
						this.fillSquare(Color.Red, robo.x, robo.y+1);
					}
					else if (map[robo.y + 1][robo.x] != 2)
					{
						this.fillSquare(Color.Aqua, robo.x, robo.y+1);
						map[robo.y + 1][robo.x] = 1;
					}
				}

				if (robo.y != 0)
				{
					up = checkIsObstacleSquare(roboCenterX, roboCenterY - cellSize);
					if (up)
					{
						map[robo.y - 1][robo.x] = 3;
						this.fillSquare(Color.Red, robo.x, robo.y-1);
					}
					else if (map[robo.y -1][robo.x] != 2)
					{
						this.fillSquare(Color.Aqua, robo.x, robo.y-1);
						map[robo.y - 1][robo.x] = 1;
					}
				}

				if (robo.x != numOfCells-1)
				{
					right = checkIsObstacleSquare(roboCenterX + cellSize, roboCenterY);
					if (right)
					{
						map[robo.y][robo.x + 1] = 3;
						this.fillSquare(Color.Red, robo.x+1, robo.y);
					}
					else if(map[robo.y][robo.x + 1] != 2)
					{
						this.fillSquare(Color.Aqua, robo.x+1, robo.y);
						map[robo.y][robo.x + 1] = 1;
					}
				}
			}
		}

		public Boolean checkIsObstacleSquare(int x, int y)
		{
			if (x > 0 && x <= pictureBox1.Width && y > 0 && y <= pictureBox1.Height)
			{
				return bmp.GetPixel(x, y).Name == HexConverter(Color.Red);
			}
			else return false;
		}

		public Boolean checkIsScanedSquare(int x, int y)
		{
			if (x > 0 && x <= pictureBox1.Width && y > 0 && y <= pictureBox1.Height)
			{
				return bmp.GetPixel(x, y).Name == HexConverter(Color.Aqua);
			}
			else return false;
		}

		private Boolean canMove()
		{
			if (stop)
			{
				return false;
			}

			int roboCenterX = robo.x * cellSize + cellSize / 2;
			int roboCenterY = robo.y * cellSize + cellSize / 2;

			switch (robo.direction)
			{
				case 0:
					if(robo.y -1 >= 0 && !checkIsObstacleSquare(roboCenterX, roboCenterY - cellSize))
					{
						if (isBasicMove && !checkIsScanedSquare(roboCenterX, roboCenterY - cellSize - cellSize))
						{
							return true;
						} else if(!isBasicMove)
						{
							return true;
						}
					} break;
				case 1:
					if (robo.x + 1 <= numOfCells - 1 && !checkIsObstacleSquare(roboCenterX + cellSize, roboCenterY))
					{
						if (isBasicMove && !checkIsScanedSquare(roboCenterX + cellSize + cellSize, roboCenterY))
						{
							return true;
						}
						else if (!isBasicMove)
						{
							return true;
						}
					}
					break;
				case 2:
					if (robo.y + 1 <= numOfCells -1 && !checkIsObstacleSquare(roboCenterX, roboCenterY + cellSize))
					{
						if (isBasicMove && !checkIsScanedSquare(roboCenterX, roboCenterY + cellSize + cellSize))
						{
							return true;
						}
						else if (!isBasicMove)
						{
							return true;
						}
					}
					break;
				case 3:
					if (robo.x -1 >= 0 && !checkIsObstacleSquare(roboCenterX - cellSize, roboCenterY))
					{
						if (isBasicMove && !checkIsScanedSquare(roboCenterX - cellSize - cellSize, roboCenterY))
						{
							return true;
						}
						else if (!isBasicMove)
						{
							return true;
						}
					}
					break;
			}

			if (isBasicMove)
			{
				robo.turnRight();
			}

			drawRobo();
			return false;
		}

		private void _pause(int value)
		{
			Stopwatch sw = new Stopwatch();
			sw.Start();
			while (sw.ElapsedMilliseconds < value)
				Application.DoEvents();
		}

		private void move()
		{
			_pause(10); // thats is used to presentate how the robot moving and discover map
			this.fillSquare(Color.Aqua, robo.x, robo.y);
			map[robo.y][robo.x] = 2;

			switch (robo.direction)
			{
				case 0:
					if (map[robo.y - 1][robo.x] != 2 || !isBasicMove)
					{
						robo.y -= 1;
					}
					else { isCycled = true; }
					break;
				case 1:
					if (map[robo.y][robo.x+1] != 2 || !isBasicMove)
					{
						robo.x += 1;
					}
					else { isCycled = true; }
					break;
				case 2:
					if (map[robo.y + 1][robo.x] != 2 || !isBasicMove)
					{
						robo.y += 1;
					}
					else { isCycled = true; }
					break;
				case 3:
					if (map[robo.y][robo.x - 1] != 2 || !isBasicMove)
					{
						robo.x -= 1;
					}
					else { isCycled = true; }
					break;
			}

			drawRobo();
		}

		private Boolean scanDone()
		{
			if (stop)
			{
				return true;
			}

			for (int i = 0; i < numOfCells; i++)
			{
				for (int j = 0; j < numOfCells; j++)
				{
					if (map[i][j] == 0)
						return false;
				}
			}
			return true;
		}

		private void printMatrix()
		{
			Console.WriteLine();
			Console.WriteLine("Scanned matrix");
			Console.WriteLine("-------------------");

			for (int i = 0; i < numOfCells; i++)
			{
				for (int j = 0; j < numOfCells; j++)
				{
					Console.Write(map[i][j]+" ");
				}
				Console.WriteLine();
			}
		}

		private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			clear();
		}

		bool isDrag = false;
		private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
		{
			isDrag = true;
		}

		private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
		{
			if(isDrag && this.mode == 1 && !isR)
			{
				MouseEventArgs me = (MouseEventArgs)e;
				Point coordinates = me.Location;
				int clickedCellX = coordinates.X / cellSize;
				int clickedCellY = coordinates.Y / cellSize;

				this.fillSquare(Color.Red, clickedCellX, clickedCellY);
			}
		}

		private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
		{
			this.isDrag = false;
		}

		private void linkLabel4_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
		{
			clear();
			this.stop = true;
		}

		private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			this.stop = true;
		}

		private dynamic FindWave(int startJ, int startI)
		{
			int targetI = startI;
			int targetJ = startJ;

			bool pathFound = false;
			int step = 0;
			int[][] waveMap = new int[numOfCells][]; // 0 - unknown area, 1 - scanned area, 2 - area with obstacle

			for (int i = 0; i < numOfCells; i++)
			{
				waveMap[i] = new int[numOfCells];
				for (int j = 0; j < numOfCells; j++)
				{
					
					switch (map[i][j])
					{
						case 3: waveMap[i][j] = -4; break; // стена 
						case 2: waveMap[i][j] = -3; break; // посещена 
						case 1: waveMap[i][j] = -2; break; // просканирована
						default: waveMap[i][j] = -1; break; // не посещена
					}
				}
			}

			waveMap[startI][startJ] = 0;

			while (!pathFound)
			{
				for (int i = 0; i < numOfCells; i++)
				{
					for (int j = 0; j < numOfCells; j++)
					{
						if (waveMap[i][j] == step)
						{
							// Проверяем, проходимы ли соседние ячейки
							if (j - 1 >= 0 && waveMap[i][j - 1] != -4) // left
							{
								// если да и соседняя ячейка не посещена, то это новая цель
								if(waveMap[i][j - 1] == -1)
								{
									targetI = i;
									targetJ = j-1;
									pathFound = true;
									waveMap[i][j - 1] = step + 1;
								}
								else if (waveMap[i][j - 1] < 0)
								{
									waveMap[i][j - 1] = step + 1;
								}
							}
							if (j + 1 < numOfCells && waveMap[i][j + 1] != -4) // right
							{
								if (waveMap[i][j + 1] == -1)
								{
									targetI = i;
									targetJ = j + 1;
									pathFound = true;
									waveMap[i][j + 1] = step + 1;
								}
								else if (waveMap[i][j + 1] < 0)
								{
									waveMap[i][j + 1] = step + 1;
								}
							}
							if (i - 1 >= 0 && waveMap[i - 1][j] != -4) // up
							{
								if (waveMap[i - 1][j] == -1)
								{
									targetI = i - 1;
									targetJ = j;
									pathFound = true;
									waveMap[i - 1][j] = step + 1;
								}
								else if (waveMap[i - 1][j] < 0)
								{
									waveMap[i - 1][j] = step + 1;
								}
							}
							if (i + 1 < numOfCells && waveMap[i + 1][j] != -4) // down
							{
								if (waveMap[i + 1][j] == -1)
								{
									targetI = i + 1;
									targetJ = j;
									pathFound = true;
									waveMap[i + 1][j] = step + 1;
								}
								else if (waveMap[i + 1][j] < 0)
								{
									waveMap[i + 1][j] = step + 1;
								}
							}
						}
					}
				}
				step++;

				if (step > numOfCells * numOfCells)
				{
					pathFound = true;
					fillUnreachableArea(waveMap);
				}
			}

			List<Cell> path = new List<Cell>();

			if (pathFound)
			{

				Cell c = new Cell {
					i = targetI,
					j = targetJ,
					weight = step
				};

				while (c.i != startI || c.j != startJ)
				{
					bool _checked = false;
					if (c.i - 1 >= 0 && waveMap[c.i - 1][c.j] == c.weight - 1) // check up
					{
						c.i = c.i - 1;
						c.weight = waveMap[c.i][c.j];
						_checked = true;
					}
					else if (c.j + 1 < numOfCells && waveMap[c.i][c.j + 1] == c.weight - 1) // check right
					{
						c.j = c.j + 1;
						c.weight = waveMap[c.i][c.j];
						_checked = true;
					}
					else if (c.i + 1 < numOfCells && waveMap[c.i + 1][c.j] == c.weight - 1) // check down
					{
						c.i = c.i + 1;
						c.weight = waveMap[c.i][c.j];
						_checked = true;
					}
					else if (c.j - 1 >= 0 && waveMap[c.i][c.j - 1] == c.weight - 1) // check left
					{
						c.j = c.j - 1;
						c.weight = waveMap[c.i][c.j];
						_checked = true;
					}

					if (_checked)
					{
						path.Add(c);
					}
				}
			}
			path.Reverse();
			if(path.Count > 0)
				path.RemoveAt(0);
			return path;
		}

		bool isR = false;

		private void button2_Click(object sender, EventArgs e)
		{
			isR = !isR;
			if (isR)
			{
				pictureBox1.Image = bmp2;

			}
			else
			{
				pictureBox1.Image = bmp;

			}
		}

		private void fillUnreachableArea(int[][] waveMap)
		{
			for (int i = 0; i < numOfCells; i++)
			{
				for (int j = 0; j < numOfCells; j++)
				{
					if (waveMap[i][j] == -1)
					{
						map[i][j] = 3;
						this.fillSquare(Color.Red, j, i);
					}
				}
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if(Int32.TryParse(textBox1.Text, out int cellC))
			{
				numOfCells = cellC;
				clear();
			}
		}
	}
	struct Cell
	{
		public int i;
		public int j;
		public int weight;
	}
}
