#define echoPin 8 // Echo Pin
#define trigPin 9 // Trigger Pin

//Setup message bytes
byte inputByte_0;
byte inputByte_1;
byte inputByte_2;

long duration, distance; // Duration used to calculate distance

void setup() {
  Serial.begin (9600);
  analogReference(INTERNAL);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
}

void loop() {
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);

  digitalWrite(trigPin, HIGH);
  delayMicroseconds(5);

  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);

  //Calculate the distance (in cm) based on the speed of sound.
  distance = duration / 58.2;

  Serial.print(distance);
  Serial.print("|");
  delay(75);
}
