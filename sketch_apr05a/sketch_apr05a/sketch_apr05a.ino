#include <Servo.h>
#define echoPin 8 // Echo Pin
#define trigPin 9 // Trigger Pin
Servo _servo;
long duration, distance; // Duration used to calculate distance
int i = 0;
void setup() {
  Serial.begin (9600);
  _servo.attach(7);
  analogReference(INTERNAL);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
}

void loop() {;
  move();
  scan();
  //_servo.write(0);
  //Serial.print("Data");
  //delay(1000);
}

void scan() {
  if(i == 0) {
   for(; i < 180; i++) {
      //_servo.write(i);
      Serial.print("&");
      Serial.print(i);  
      getDistance();
   }
  } else {
    for(; i > 0; i--) {
      //_servo.write(i);
      Serial.print("&");
      Serial.print(i);  
      getDistance();
    }
  }
}

void getDistance() {
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);

  digitalWrite(trigPin, HIGH);
  delayMicroseconds(5);

  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);

  //Calculate the distance (in cm) based on the speed of sound.
  distance = duration / 58.2;
  if(distance < 300) {
    Serial.print("|"); 
    Serial.print(distance);
  } else {
    Serial.print("|"); 
    Serial.print(0);
  }
}

void move() {
  delay(3000);
}


