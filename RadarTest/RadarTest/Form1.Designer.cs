﻿namespace RadarTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.AngleValuelb = new System.Windows.Forms.Label();
			this.DistanceValuelb = new System.Windows.Forms.Label();
			this.startlink = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.Connectionstatus = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// pictureBox1
			// 
			this.pictureBox1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.pictureBox1.Location = new System.Drawing.Point(1, 2);
			this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(1004, 834);
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			// 
			// timer1
			// 
			this.timer1.Interval = 5;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick_1);
			// 
			// AngleValuelb
			// 
			this.AngleValuelb.AutoSize = true;
			this.AngleValuelb.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.AngleValuelb.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.AngleValuelb.ForeColor = System.Drawing.Color.Lime;
			this.AngleValuelb.Location = new System.Drawing.Point(833, 110);
			this.AngleValuelb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.AngleValuelb.Name = "AngleValuelb";
			this.AngleValuelb.Size = new System.Drawing.Size(0, 28);
			this.AngleValuelb.TabIndex = 4;
			// 
			// DistanceValuelb
			// 
			this.DistanceValuelb.AutoSize = true;
			this.DistanceValuelb.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.DistanceValuelb.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.DistanceValuelb.ForeColor = System.Drawing.Color.Lime;
			this.DistanceValuelb.Location = new System.Drawing.Point(833, 55);
			this.DistanceValuelb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.DistanceValuelb.Name = "DistanceValuelb";
			this.DistanceValuelb.Size = new System.Drawing.Size(0, 28);
			this.DistanceValuelb.TabIndex = 5;
			// 
			// startlink
			// 
			this.startlink.AutoSize = true;
			this.startlink.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.startlink.Font = new System.Drawing.Font("Consolas", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.startlink.ForeColor = System.Drawing.Color.Lime;
			this.startlink.Location = new System.Drawing.Point(13, 9);
			this.startlink.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.startlink.Name = "startlink";
			this.startlink.Size = new System.Drawing.Size(77, 28);
			this.startlink.TabIndex = 7;
			this.startlink.Text = "Start";
			this.startlink.Click += new System.EventHandler(this.startlink_Click);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label5.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.ForeColor = System.Drawing.Color.Lime;
			this.label5.Location = new System.Drawing.Point(98, 9);
			this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(117, 20);
			this.label5.TabIndex = 8;
			this.label5.Text = "Connection :";
			// 
			// Connectionstatus
			// 
			this.Connectionstatus.AutoSize = true;
			this.Connectionstatus.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.Connectionstatus.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Connectionstatus.ForeColor = System.Drawing.Color.Lime;
			this.Connectionstatus.Location = new System.Drawing.Point(223, 9);
			this.Connectionstatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.Connectionstatus.Name = "Connectionstatus";
			this.Connectionstatus.Size = new System.Drawing.Size(171, 20);
			this.Connectionstatus.TabIndex = 9;
			this.Connectionstatus.Text = "Not Conneccted....";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.label4.Font = new System.Drawing.Font("Consolas", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.ForeColor = System.Drawing.Color.Lime;
			this.label4.Location = new System.Drawing.Point(13, 55);
			this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(77, 28);
			this.label4.TabIndex = 10;
			this.label4.Text = "Clean";
			this.label4.Click += new System.EventHandler(this.label4_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1004, 839);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.Connectionstatus);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.startlink);
			this.Controls.Add(this.DistanceValuelb);
			this.Controls.Add(this.AngleValuelb);
			this.Controls.Add(this.pictureBox1);
			this.Margin = new System.Windows.Forms.Padding(4);
			this.Name = "Form1";
			this.Text = "Ultrasonic Radar";
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label AngleValuelb;
        private System.Windows.Forms.Label DistanceValuelb;
        private System.Windows.Forms.Label startlink;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label Connectionstatus;
        private System.Windows.Forms.Label label4;
	}
}

