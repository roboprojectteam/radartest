﻿using System;
using System.Drawing;
using System.Windows.Forms;

using System.IO.Ports;
using System.Threading;

namespace RadarTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                pictureBox1.Invalidate();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            timer1.Start();
        }
		
        SolidBrush brush = new SolidBrush(Color.LimeGreen);
		
		static string buffer = String.Empty;
		static int bufferIndex = 0;

		private void timer1_Tick_1(object sender, EventArgs e)
		{
			if (buffer != String.Empty)
			{
				int _length = buffer.LastIndexOf('&') - bufferIndex;
				string _buffer = buffer.Substring(bufferIndex, _length);
				bufferIndex += _buffer.Length;
				string[] _data = _buffer.Split(new[] { '&' }, StringSplitOptions.RemoveEmptyEntries);
				for (int i = 0; i < _data.Length; i++)
				{
					string[] __data = _data[i].Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
					if(__data.Length == 2)
					{
						int angle = Convert.ToInt32(__data[0]);
						int distance = Convert.ToInt32(__data[1]);

						if(distance != 0 && distance < 300 && angle >= 0 && angle <= 180)
						{
							double dX = distance * Math.Cos(Math.PI * angle / 180.0);
							double dY = distance * Math.Sin(Math.PI * angle / 180.0);
							int _dX = Convert.ToInt32(dX);
							int _dY = Convert.ToInt32(dY);
							drawPoint(pictureBox1.ClientRectangle.Width / 2 + _dX, pictureBox1.ClientRectangle.Height / 2 + _dY);
						}
					}
				}
			}
        }

		static SerialPort currentPort;
		public Boolean connect(int baud = 9600)
		{
			try
			{
				currentPort = new SerialPort("COM8", baud);

				currentPort.Parity = Parity.None;
				currentPort.StopBits = StopBits.One;
				currentPort.DataBits = 8;
				currentPort.Handshake = Handshake.None;

				Connectionstatus.Text = "Connection Successful - Connected to  " + currentPort.PortName;

				currentPort.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);

				currentPort.Open();

				return true;
			}
			catch (Exception e)
			{
				return false;
			}
		}

		private static void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
		{
			SerialPort sp = (SerialPort)sender;
			string data = sp.ReadExisting();

			buffer += data;

			if (buffer.Length > 50000)
			{
				string resizeBuffer = buffer.Substring(0, 50000);
				buffer = buffer.Remove(0, resizeBuffer.LastIndexOf('|') + 1);
				bufferIndex -= resizeBuffer.LastIndexOf('|');
			}
		}

		public void drawPoint(int x, int y)
        {
            Graphics g = Graphics.FromHwnd(pictureBox1.Handle);
            Point dPoint = new Point(x, (pictureBox1.Height - y));
            dPoint.X = dPoint.X - 1;
            dPoint.Y = dPoint.Y - 1;
            Rectangle rect = new Rectangle(dPoint, new Size(1,1));
            g.FillRectangle(brush, rect);
            g.Dispose();
        }

        private void startlink_Click(object sender, EventArgs e)
        {
			this.connect();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            pictureBox1.Refresh();
			currentPort.Close();
		}
	}
}
