﻿namespace RadarTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.AngleValuelb = new System.Windows.Forms.Label();
            this.DistanceValuelb = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.startlink = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Connectionstatus = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelpink = new System.Windows.Forms.Label();
            this.labelblue = new System.Windows.Forms.Label();
            this.labelred = new System.Windows.Forms.Label();
            this.dotsLines = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox1.Location = new System.Drawing.Point(1, 2);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(753, 678);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Interval = 130;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Lime;
            this.label1.Location = new System.Drawing.Point(505, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 22);
            this.label1.TabIndex = 1;
            this.label1.Text = "Distance :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Lime;
            this.label2.Location = new System.Drawing.Point(505, 141);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 22);
            this.label2.TabIndex = 2;
            this.label2.Text = "Reading Speed :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Lime;
            this.label3.Location = new System.Drawing.Point(505, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 22);
            this.label3.TabIndex = 3;
            this.label3.Text = "Angle :";
            // 
            // AngleValuelb
            // 
            this.AngleValuelb.AutoSize = true;
            this.AngleValuelb.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.AngleValuelb.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AngleValuelb.ForeColor = System.Drawing.Color.Lime;
            this.AngleValuelb.Location = new System.Drawing.Point(625, 89);
            this.AngleValuelb.Name = "AngleValuelb";
            this.AngleValuelb.Size = new System.Drawing.Size(0, 22);
            this.AngleValuelb.TabIndex = 4;
            // 
            // DistanceValuelb
            // 
            this.DistanceValuelb.AutoSize = true;
            this.DistanceValuelb.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.DistanceValuelb.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DistanceValuelb.ForeColor = System.Drawing.Color.Lime;
            this.DistanceValuelb.Location = new System.Drawing.Point(625, 45);
            this.DistanceValuelb.Name = "DistanceValuelb";
            this.DistanceValuelb.Size = new System.Drawing.Size(0, 22);
            this.DistanceValuelb.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label6.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Lime;
            this.label6.Location = new System.Drawing.Point(661, 141);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 22);
            this.label6.TabIndex = 6;
            this.label6.Text = "130Ms";
            // 
            // startlink
            // 
            this.startlink.AutoSize = true;
            this.startlink.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.startlink.Font = new System.Drawing.Font("Consolas", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startlink.ForeColor = System.Drawing.Color.Lime;
            this.startlink.Location = new System.Drawing.Point(542, 186);
            this.startlink.Name = "startlink";
            this.startlink.Size = new System.Drawing.Size(150, 22);
            this.startlink.TabIndex = 7;
            this.startlink.Text = "Click To Start";
            this.startlink.Click += new System.EventHandler(this.startlink_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Lime;
            this.label5.Location = new System.Drawing.Point(285, 658);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "Connection :";
            // 
            // Connectionstatus
            // 
            this.Connectionstatus.AutoSize = true;
            this.Connectionstatus.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Connectionstatus.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Connectionstatus.ForeColor = System.Drawing.Color.Lime;
            this.Connectionstatus.Location = new System.Drawing.Point(382, 658);
            this.Connectionstatus.Name = "Connectionstatus";
            this.Connectionstatus.Size = new System.Drawing.Size(133, 15);
            this.Connectionstatus.TabIndex = 9;
            this.Connectionstatus.Text = "Not Conneccted....";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Font = new System.Drawing.Font("Consolas", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Lime;
            this.label4.Location = new System.Drawing.Point(543, 221);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 22);
            this.label4.TabIndex = 10;
            this.label4.Text = "Clean";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label7.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Lime;
            this.label7.Location = new System.Drawing.Point(505, 318);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(220, 22);
            this.label7.TabIndex = 11;
            this.label7.Text = "Change Drawing Color:";
            // 
            // labelpink
            // 
            this.labelpink.AutoSize = true;
            this.labelpink.BackColor = System.Drawing.Color.Magenta;
            this.labelpink.Font = new System.Drawing.Font("Consolas", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelpink.ForeColor = System.Drawing.Color.Magenta;
            this.labelpink.Location = new System.Drawing.Point(609, 351);
            this.labelpink.Name = "labelpink";
            this.labelpink.Size = new System.Drawing.Size(45, 19);
            this.labelpink.TabIndex = 12;
            this.labelpink.Text = "Pink";
            this.labelpink.Click += new System.EventHandler(this.labelpink_Click);
            // 
            // labelblue
            // 
            this.labelblue.AutoSize = true;
            this.labelblue.BackColor = System.Drawing.Color.Aqua;
            this.labelblue.Font = new System.Drawing.Font("Consolas", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelblue.ForeColor = System.Drawing.Color.Aqua;
            this.labelblue.Location = new System.Drawing.Point(661, 351);
            this.labelblue.Name = "labelblue";
            this.labelblue.Size = new System.Drawing.Size(45, 19);
            this.labelblue.TabIndex = 13;
            this.labelblue.Text = "Blue";
            this.labelblue.Click += new System.EventHandler(this.labelblue_Click);
            // 
            // labelred
            // 
            this.labelred.AutoSize = true;
            this.labelred.BackColor = System.Drawing.Color.Red;
            this.labelred.Font = new System.Drawing.Font("Consolas", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelred.ForeColor = System.Drawing.Color.Red;
            this.labelred.Location = new System.Drawing.Point(567, 351);
            this.labelred.Name = "labelred";
            this.labelred.Size = new System.Drawing.Size(36, 19);
            this.labelred.TabIndex = 14;
            this.labelred.Text = "Red";
            this.labelred.Click += new System.EventHandler(this.labelred_Click);
            // 
            // dotsLines
            // 
            this.dotsLines.AutoSize = true;
            this.dotsLines.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dotsLines.Font = new System.Drawing.Font("Consolas", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dotsLines.ForeColor = System.Drawing.Color.Lime;
            this.dotsLines.Location = new System.Drawing.Point(543, 253);
            this.dotsLines.Name = "dotsLines";
            this.dotsLines.Size = new System.Drawing.Size(110, 22);
            this.dotsLines.TabIndex = 15;
            this.dotsLines.Text = "Show Lines";
            this.dotsLines.Click += new System.EventHandler(this.label8_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label9.Font = new System.Drawing.Font("Consolas", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Lime;
            this.label9.Location = new System.Drawing.Point(546, 287);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(150, 22);
            this.label9.TabIndex = 16;
            this.label9.Text = "Show Diffrence";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(753, 682);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dotsLines);
            this.Controls.Add(this.labelred);
            this.Controls.Add(this.labelblue);
            this.Controls.Add(this.labelpink);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Connectionstatus);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.startlink);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.DistanceValuelb);
            this.Controls.Add(this.AngleValuelb);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Ultrasonic Radar";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label AngleValuelb;
        private System.Windows.Forms.Label DistanceValuelb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label startlink;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label Connectionstatus;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelpink;
        private System.Windows.Forms.Label labelblue;
        private System.Windows.Forms.Label labelred;
        private System.Windows.Forms.Label dotsLines;
        private System.Windows.Forms.Label label9;

    }
}

