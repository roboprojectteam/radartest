﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RadarTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {

                pictureBox1.Invalidate();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            timer1.Start();
        }
      
        private void timer1_Tick(object sender, EventArgs e)
        {
            
        }
        int degree = 0;
        int distance = 0;
        Boolean dots = true;
        Boolean diffrence = false;
        communicator comport = new communicator();
        
        Boolean portConnection = false;

        int[] positionArray = new int[280];


        private void timer1_Tick_1(object sender, EventArgs e)
        {
            String incomming = comport.message(4, 8, 32);
            if (incomming.IndexOf("-") != -1)
            {
               
                try
                {
                    DistanceValuelb.Text = incomming.Substring(0, incomming.IndexOf("-"));
                    AngleValuelb.Text = incomming.Substring(incomming.IndexOf("-") + 1, (incomming.Length - 2) - incomming.IndexOf("-"));

                    distance = Convert.ToInt32(DistanceValuelb.Text)+50;
                    degree = Convert.ToInt32(AngleValuelb.Text);
                }
                catch
                { }
            } 
            
            var halfX = pictureBox1.ClientRectangle.Width / 2;
            var halfY = pictureBox1.ClientRectangle.Height / 2;

            int xposition = Convert.ToInt32((distance * Math.Sin((double)degree * 0.0174532925)));
            int yposition = Convert.ToInt32(halfY + distance * Math.Cos((double)degree * 0.0174532925));


            if (diffrence == true)
            {
                if (positionArray[degree] == 0)
                {
                    brush.Color = Color.LimeGreen;
                }
                else if ((positionArray[degree] - distance) >= 20 || (positionArray[degree] - distance) <= -20)
                {
                    brush.Color = Color.Red;
                }
            }
            positionArray[degree] = distance;
            


            drawPoint(xposition, yposition);
            if (dots == false)
            {
                drawLine(distance, degree);
            }
            //pictureBox1.Refresh();
        }
        SolidBrush brush = new SolidBrush(Color.LimeGreen);
        
        public void drawLine(int distance, int degree)
        {
            Graphics g = Graphics.FromHwnd(pictureBox1.Handle);
            var halfY = pictureBox1.ClientRectangle.Height / 2;
            Pen pen = new Pen(brush);
            g.DrawLine(pen, Convert.ToInt32((distance * Math.Sin((double)degree * 0.0174532925))),pictureBox1.Height- Convert.ToInt32(halfY + distance * Math.Cos((double)degree * 0.0174532925)), Convert.ToInt32((2000 * Math.Sin((double)degree * 0.0174532925))),pictureBox1.Height- Convert.ToInt32(halfY + 2000 * Math.Cos((double)degree * 0.0174532925)));
        }
        public void drawPoint(int x, int y)
        {
            Graphics g = Graphics.FromHwnd(pictureBox1.Handle);
            Point dPoint = new Point(x, (pictureBox1.Height - y));
            dPoint.X = dPoint.X - 1;
            dPoint.Y = dPoint.Y - 1;
            Rectangle rect = new Rectangle(dPoint, new Size(3,3));
            g.FillRectangle(brush, rect);
            g.Dispose();
        }

        private void startlink_Click(object sender, EventArgs e)
        {
            if (comport.connect(9600, "I'M ARDUINO", 4, 8, 16))
            {
                Connectionstatus.Text = "Connection Successful - Connected to  " + comport.port;
                portConnection = true;
                comport.connect(9600, "0", 4, 8, 8);
                timer1.Start();
            }
            else
            {
                Connectionstatus.Text = "Not connected . . . ";
                portConnection = false;
                timer1.Stop();
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {
            pictureBox1.Refresh();
            Array.Clear(positionArray, 0, positionArray.Length);
            brush.Color = Color.LimeGreen;
        }

        private void labelred_Click(object sender, EventArgs e)
        {
            brush.Color = Color.Red;
        }

        private void labelpink_Click(object sender, EventArgs e)
        {
            brush.Color = Color.Magenta;
        }

        private void labelblue_Click(object sender, EventArgs e)
        {
            brush.Color = Color.Aqua;
        }

        private void label8_Click(object sender, EventArgs e)
        {
            if (dots == true)
            {
                dots = false;
                dotsLines.Text = "Show Dots Only";

            }
            else {
            dots = true;
                dotsLines.Text ="Show Lines";}
        }

        private void label9_Click(object sender, EventArgs e)
        {
            if (diffrence == true)
            {
                label9.Text = "Show Diffrence";
                diffrence = false;

            }
            else
            {
                label9.Text =  "Hide Diffrence";
                diffrence = true;
            }
        }
        
    }
}
